### What is it? ###

Helping Boo is a 3D computer game built with the *three.js* library. It uses the programming language JavaScript and is based on WebGL and html5 canvas.   

### How do I get set up? ###
For Windows:

* Make sure you have the Microsoft DirectX runtime installed.
* Make sure that your graphic cards are on the latest versions of drivers.
* Make sure that your browsers are on the latest version.

### Seeing it live ###
You can check it out at http://kelleee.bitbucket.org/helpingboo

* Warning: The graphics may differ for different computers, depending on your graphic card. 

### Running it locally ###
If you wish to run this program locally:

* Clone this repository: ```git clone https://kelleee@bitbucket.org/kelleee/helping-boo.git```
* Open up a terminal, navigate to the cloned repository and run a local server (I used a Python server for development).
* Next, choose your browser and navigate to localhost:8000 (for Python server) or localhost:3000 (for JavaScript server).  
