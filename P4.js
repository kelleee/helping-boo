/**
 * UBC CPSC 314, January 2016
 * Project 4
 */

var scene = new THREE.Scene();
scene.fog = new THREE.Fog( 0xffffff, 1, 5000 );
scene.fog.color.setHSL( 0.6, 0, 1);

var clock = new THREE.Clock();

// SETUP RENDERER
var renderer = new THREE.WebGLRenderer( { antialias: false } );
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.setClearColor( scene.fog.color );
renderer.shadowMap.cullFace = THREE.CullFaceBack;
document.body.appendChild(renderer.domElement);

// SETUP CAMERA
var aspect = window.innerWidth/window.innerHeight;
var camera = new THREE.PerspectiveCamera( 45, aspect, 0.1, 10000 );
camera.position.set( 80, 45, -80 );
camera.lookAt( scene.position ); 
scene.add(camera);

// SETUP ORBIT CONTROL OF THE CAMERA
var controls = new THREE.OrbitControls(camera);
controls.damping = 0.2;
controls.maxPolarAngle = 0.9 * Math.PI / 2;

// ADAPT TO WINDOW RESIZE
function resize() {
  renderer.setSize(window.innerWidth, window.innerHeight);
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
}

window.addEventListener('resize', resize);
resize();

// CREATE 2D MAP
var map = new Array(100);
for (var i = 0; i < 100; i++) {
  map[i] = new Array(100);
  for (var j = 0; j < 100; j++) {
    map[i][j] = 0;
  }
}

// LIGHTS
var light = new THREE.DirectionalLight( 0xffffff, 1.0 );
light.position.set( 300, 500, -600 );
light.castShadow = true;
light.shadowDarkness = 0.5;
scene.add(light);

var spotLight = new THREE.SpotLight( 0xffffff );
spotLight.position.set( 0, 30, 0);
scene.add( spotLight );


// LIGHTING UNIFORMS
var topColor = new THREE.Color( 0x0077ff );
var bottomColor = new THREE.Color( 0xffffff );


// SKY
var vertexShader = document.getElementById( 'sky-vs' ).textContent;
var fragmentShader = document.getElementById( 'sky-fs' ).textContent;
var uniforms = {
    topColor: { type : "c", value : topColor },
    bottomColor: { type : "c", value : bottomColor },
    offset: { type: "f", value: 500 },
    exponent: { type: "f", value: 0.60 }
};

var skyGeometry = new THREE.SphereGeometry( 4000, 32, 15 );
var skyMaterial = new THREE.ShaderMaterial( {
  uniforms: uniforms,
  vertexShader: vertexShader,
  fragmentShader: fragmentShader,
  side: THREE.BackSide
});
var sky = new THREE.Mesh( skyGeometry, skyMaterial );
scene.add( sky );

// FLOOR 
var floorTexture = new THREE.ImageUtils.loadTexture('images/grass.jpg');
floorTexture.wrapS = floorTexture.wrapT = THREE.RepeatWrapping;
floorTexture.repeat.set(1000, 1000);

var floorMaterial = new THREE.MeshBasicMaterial({ map: floorTexture, side: THREE.DoubleSide });

// GROUND
var groundGeometry = new THREE.PlaneBufferGeometry( 10000, 10000 );
var groundMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x050505 } );
groundMaterial.color.setHSL( 0.095, 0.7, 0.75 );
groundMaterial.color.setHex( 0xd5c67b );

var ground = new THREE.Mesh( groundGeometry, floorMaterial );
ground.rotation.x = -Math.PI/2;
ground.position.y = -1;

scene.add( ground );
ground.receiveShadow = true;


// OTHER TEXTURES
var crateTexture = new THREE.ImageUtils.loadTexture('images/crate.png');
var stoneTexture = new THREE.ImageUtils.loadTexture('images/stone.jpg');
stoneTexture.wrapS = stoneTexture.wrapT = THREE.RepeatWrapping;
stoneTexture.repeat.set(10,1);


// MATERIALS
var defaultMaterial = new THREE.MeshLambertMaterial();
var crateMaterial = new THREE.MeshBasicMaterial({ map: crateTexture });
var stoneMaterial = new THREE.MeshBasicMaterial({ map: stoneTexture });
var torsoMaterial = new THREE.MeshPhongMaterial({ color: 0xfff3e5 });
var headMaterial  = new THREE.MeshPhongMaterial({ color: 0x051249 });
var darkMaterial  = new THREE.MeshPhongMaterial({ color: 0x2f312c });
var blueMaterial   = new THREE.MeshPhongMaterial({ color: 0x5169a7 });


// PLAYER 
// BODY
var torsoGeometry = new THREE.BoxGeometry( 2, 3, 2 );
var body = new THREE.Mesh( torsoGeometry, torsoMaterial );
body.position.set( 0, 3, -200 );
body.direction = new THREE.Vector3( 0, 0, 0 );
scene.add( body );

// var beltGeometry = new THREE.BoxGeometry( 2.2, 1, 2.2);
// var belt = new THREE.Mesh( beltGeometry, defaultMaterial );
// body.add( belt );
// HEAD
var headGeometry = new THREE.BoxGeometry( 4, 4, 4 );
var head = new THREE.Mesh( headGeometry, torsoMaterial );
head.position.set( 0, 4, 0 );
body.add( head );

// NOSE
var noseGeometry = new THREE.SphereGeometry( 0.2, 32, 32 );
var nose = new THREE.Mesh( noseGeometry, defaultMaterial );
nose.position.set( 0, 0, 2);
head.add( nose );

// ARMS
var armGeometry = new THREE.CylinderGeometry( 0.5, 0.3, 2, 32 );
var arms = {
  right: new THREE.Mesh( armGeometry, blueMaterial ),
  left: new THREE.Mesh( armGeometry, blueMaterial )
}
arms.right.position.set( -2, 1, 0);
arms.left.position.set( 2, 1, 0);
body.add( arms.right );
body.add( arms.left );

// HANDS
var handGeometry = new THREE.SphereGeometry( 0.3 , 32, 32 );
var hands = {
  right: new THREE.Mesh( handGeometry, darkMaterial ),
  left: new THREE.Mesh( handGeometry, darkMaterial )
}
hands.right.position.set( 0, -1.2, 0 );
hands.left.position.set( 0, -1.2, 0 );
arms.right.add( hands.right );
arms.left.add( hands.left );

// LEGS
var legGeometry = new THREE.CylinderGeometry( 0.3, 0.5, 2, 32 );
var legs = {
  right: new THREE.Mesh( legGeometry, darkMaterial ),
  left: new THREE.Mesh( legGeometry, darkMaterial )
}
legs.right.position.set( -0.5, -2, 0 );
legs.left.position.set( 0.5, -2, 0 );
body.add( legs.right );
body.add( legs.left );

var pantGeometry = new THREE.BoxGeometry( 1, 1, 1 );
var pants = {
  right: new THREE.Mesh( pantGeometry, blueMaterial ),
  left: new THREE.Mesh( pantGeometry, blueMaterial )
}
legs.right.add( pants.right );
legs.left.add( pants.left );


// CREATE ENEMIES
var enemies = [];
var radius = 500;
var nEnemies = 5;

var headGeometry  = new THREE.SphereGeometry( 3, 16, 32 );
var bodyGeometry  = new THREE.CylinderGeometry( 3, 3, 4, 8 );
var eyeGeometry   = new THREE.SphereGeometry( 1, 16, 32 );
for ( var i = 0; i < nEnemies; i++ ) {
  var color = Math.random() * 0xffffff;

  var enemy = new THREE.Mesh( headGeometry, new THREE.MeshLambertMaterial( { color: color} ) );
  enemy.position.set( radius/2 - radius * Math.random(),
                      5.0,
                      radius/2 - radius * Math.random() );
  enemy.name = "enemy";

  var bodyE = new THREE.Mesh( bodyGeometry, new THREE.MeshLambertMaterial( { color: color } ) );
  bodyE.position.set(0, -1.5, 0);
  enemy.add( bodyE );
  
  var eyes = {
    right: new THREE.Mesh( eyeGeometry, new THREE.MeshLambertMaterial( { color: 0x0ffffff} ) ),
    left: new THREE.Mesh( eyeGeometry, new THREE.MeshLambertMaterial( { color: 0xffffff} ) )
  };
  eyes.right.position.set( -1, 0, 2 );
  eyes.left.position.set( 1, 0, 2 );
  
  enemy.add( eyes.right );
  enemy.add( eyes.left );
  
  enemy.direction = new THREE.Vector3();
  enemy.hitbox = new THREE.Box3();

  scene.add( enemy );
  enemies.push( enemy );
}


// CREATE BOXES
var nBoxes = 10;
var boxes = [];

var boxGeometry = new THREE.BoxGeometry( 3, 3, 3 );
for ( var i = 0; i < nBoxes; i++ ) {
  var box = new THREE.Mesh( boxGeometry, crateMaterial );
  box.position.set( radius/2 - radius * Math.random(),
                    3.0,
                    radius/2 - radius * Math.random());
  scene.add( box );
  boxes.push( box );
}


// CREATE TREES
var trees = [];
var nTrees = 120;

var trunkGeometry = new THREE.CylinderGeometry( 0.5, 1, 15 );  
var trunkMaterial = new THREE.MeshPhongMaterial( { color: 0x7a5230 } );
var foliageGeometry = new THREE.SphereGeometry(5, 18, 8);
var foliageMaterial = new THREE.MeshPhongMaterial({ 
    color: 0x4C5A30, 
    shading: THREE.FlatShading 
});
for ( var i = 0; i < nTrees; i++ ) {
  var trunk = new THREE.Mesh( trunkGeometry, trunkMaterial );
  trunk.name = "tree";
  trunk.position.set( radius/2 - radius * Math.random(),
                      6.0,
                      radius/2 - radius * Math.random());

  var foliage = new THREE.Mesh( foliageGeometry, foliageMaterial );
  foliage.position.set( 0, 10.0, 0 );
  
  trunk.add(foliage);
  
  trees.push( trunk );
  scene.add( trunk );
}


// CREATE EXIT DOOR
var doorGeometry = new THREE.BoxGeometry( 10, 1000, 10 );
var doorMaterial =  new THREE.MeshLambertMaterial({ 
  color: 0xe7db9d,
  transparent: true
});
doorMaterial.opacity = 0.5;

var door = new THREE.Mesh( doorGeometry, doorMaterial );
door.name = "door";
door.position.set( 0, 500, 0);
scene.add( door );


// // CREATE SWORD 
// var swordGeometry = new THREE.CylinderGeometry( 0.1, 0.05, 5 );
// var sword = new THREE.Mesh( swordGeometry, new THREE.MeshLambertMaterial( { color: 0xffffff} ) );
// sword.position.set( 0, -2, 0);


// // CREATE BULLETS
// var bulletGeometry = new THREE.SphereGeometry( 0.5, 32, 32 );
// var bulletMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, shading: THREE.FlatShading } );
// var bullet = new THREE.Mesh( bulletGeometry, bulletMaterial);
// //bullet.position.set( 0, -1, 0 );


// LOAD AUDIO
var audio = document.createElement('audio');
var source = document.createElement('source');
source.src = 'sounds/shooting-stars.mp3'
audio.appendChild( source );

var audio2 = document.createElement('audio');
var source2 = document.createElement('source');
source2.src = 'sounds/zombie.mp3';
audio2.appendChild( source2 );



// FUNCTIONS
// REF: http://webmaestro.fr/character-controls-three-js/
/**
 * Set Direction vector of Character
 */
function setDirection( controller ) {
  var x = controller.left? 1 : controller.right ? -1 : 0;
  var y = 0;
  var z = controller.up? 1 : controller.down ? -1 : 0;

  body.direction.set( x, y, z );
}

/**
 * Rotate Character
 */
function rotate( character ) {
  var angle = Math.atan2(body.direction.x, body.direction.z);
  var diff = angle - body.rotation.y;

  if ( Math.abs( diff ) > Math.PI ) {
    if ( diff > 0 ) {
      body.rotation.y += 2 * Math.PI;
    } else {
      body.rotation.y -= 2 * Math.PI;
    }

    diff = angle - body.rotation.y;
  }

  if ( diff !== 0 ) {
    body.rotation.y += diff/4;
  }
}

/**
 * Character motion
 */
var step = 0;
function moveCharacter() { 
  step += 1/4;
  legs.left.rotateX( 0.1 * Math.sin(step) );
  legs.right.rotateX( 0.1 * Math.cos(step + Math.PI/4) );
  arms.left.rotateX( 0.1 * Math.cos(step + Math.PI/4) );
  arms.right.rotateX( 0.1 * Math.sin(step) );
  body.translateZ( 0.5 );

}

/**
 * Collision Detections using THREE.Raycaster()
 */

body.raycaster = new THREE.Raycaster(); // hook raycaster to the player
for ( var i = 0; i < enemies.length; i++ ) {  // hook raycaster to all enemies
  enemies[i].raycaster = new THREE.Raycaster();
}

// initialize all possible directions
var rayDirections = [
  new THREE.Vector3(0, 0, 1),
  new THREE.Vector3(1, 0, 1),
  new THREE.Vector3(1, 0, 0),
  new THREE.Vector3(1, 0, -1),
  new THREE.Vector3(0, 0, -1),
  new THREE.Vector3(-1, 0, -1),
  new THREE.Vector3(-1, 0, 0),
  new THREE.Vector3(-1, 0, 1)
]

var lives = 3;
var state = 0;
function collision() {

  body.raycaster.set( body.position, rayDirections );
  
  for ( var i = 0; i < rayDirections.length; i++ ) {  

    // Check collisions between player and gems
    body.raycaster.set( body.position, rayDirections[i] );

    var objects = boxes.concat( door );
    var intersects = body.raycaster.intersectObjects( objects );
    if ( intersects.length > 0 && intersects[0].distance < 2) {

      if ( intersects[0].object.name === "door" ) {
        console.log(boxes.length);
        if ( boxes.length ) {
          document.getElementById( 'message' ).innerHTML = "You must collect all of the items."
        } else if ( !boxes.length ){
          document.getElementById( 'message' ).innerHTML = "Congratulations! It's time to go home."
          state = 1;
          update = false;
          body.position.x = 0;
          body.position.z = 0;
          body.position.y += 0.01;
        }
      }

      for ( var j = 0; j < boxes.length; j++ ) {
        if ( boxes[j].position == intersects[0].object.position ) {
          scene.remove( boxes[j] ); 
          boxes.splice(j, 1); 
          document.getElementById( 'message' ).innerHTML = nBoxes - boxes.length + " items collected.";
          document.getElementById( 'items' ).innerHTML = nBoxes - boxes.length + "/ 10 ITEMS COLLECTED";
          audio.play();
          if ( boxes.length == 0 ) {
            document.getElementById( 'message' ).innerHTML = "You've collected all of the items. Now, you may exit though the door."
          } 
        }
      }

    }
    
    // Check collisions between player and enemies and trees
    var collisions = body.raycaster.intersectObjects( enemies.concat(trees) );
    if ( collisions.length > 0 && collisions[0].distance < 2 ) {

      if ((i === 0 || i === 1 || i === 7) && body.direction.z === 1 ) {
        body.direction.setZ( 0 );
      } else if ((i === 3 || i === 4 || i === 5) && body.direction.z === -1 ) {
        body.direction.setZ( 0 );
      } 

      if ((i === 1 || i === 2 || i === 3) && body.direction.x === 1 ) {
        body.direction.setX( 0 );
      } else if ((i === 5 || i === 6 || i === 7) &&  body.direction.x === -1 ) {
        body.direction.setX( 0 );
      }

      if ( collisions[0].object.name === "enemy") {
        lives--;

        document.getElementById( 'hit' ).style.display = 'block';
        
        document.getElementById( 'message' ).innerHTML = "Ouch! You have " + lives + " lives left."
        document.getElementById( 'lives' ).innerHTML = lives + " / 3 LIVES";
        
        setTimeout( function (){
          document.getElementById( 'hit' ).style.display = 'none';
        }, 1000 );
        
        body.position.set( 0, 3, -200 );
        
        if (lives == 0) {
          update = false;
          document.getElementById( 'count' ).innerHTML = "GAME OVER";
          document.getElementById( 'panel' ).style.display = 'none'
          document.getElementById( 'count' ).style.display = 'block';
          document.getElementById( 'menu' ).style.display = 'block';
        }
      } else {
        document.getElementById( 'message' ).innerHTML = "Oops, you hit a tree."
        
      }
    }
    
    var rad = 10;
    // Check collisions between enemies, if too close, separate them
    for ( var j = 0; j < enemies.length; j++ ) {
      enemies[j].raycaster.set( enemies[j].position, rayDirections[i] );
      var inter = enemies[j].raycaster.intersectObjects( enemies );
      if (inter.length > 0 && inter[0].distance < 4) {
        enemies[j].position.z += rad * 2;
        enemies[j].position.x += rad * 2;
     }
    }
  }

}


/**
 * Spin boxes on their own axis
 */

var axis = new THREE.Vector3( 0, 1, 0 );

function spinOnAxis () {
  for ( var i = 0; i < boxes.length; i++ ) {
    boxes[i].rotateOnAxis( axis, 0.02 );
  }
}

/**
 * Enemies AI to chase player when distance is close
 */
function moveEnemies () {
  var x1, z1, x2, z2;

  x2 = body.position.x;
  z2 = body.position.z;

  for (var i = 0; i < enemies.length; i++) {
    
    enemies[i].lookAt(body.position);
    
    x1 = enemies[i].position.x;
    z1 = enemies[i].position.z;
    
    var distanceX = Math.abs(x1 - x2);
    var distanceZ = Math.abs(z1 - z2);
    
    if ( distanceX < 0 || distanceZ < 0 ) {
         enemies[i].position.x = enemies[i].position.x;
         enemies[i].position.z = enemies[i].position.z;
    } else if (distanceX < 50 && distanceZ < 50) {
      audio2.play();
      document.getElementById( 'message' ).innerHTML = "Watch out! Some monster is approaching."
      enemies[i].position.x -= (x1 - x2) * 0.008;
      enemies[i].position.z -= (z1 - z2) * 0.008;
    } 
  }
}


// function compass () {
//   var boxx = boxes[ boxes.length - 1];
//   var dir = new THREE.Vector2();
//   var dx = body.position.x - boxx.position.x;
//   var dz = body.position.z - boxx.position.z;
//   dir.set(dx, dz).normalize();
//   console.log(dir);
// }


// KEYBOARD CONTROLLER
var controller = {
  up: false,
  down: false,
  left: false,
  right: false,
  hit: false,
}

// SETUP UPDATE CALL-BACK
var keyboard = new THREEx.KeyboardState();
function onKeyDown( event ) {

    // set direction
    if( keyboard.eventMatches(event,"s") ) {
      controller.down = true; 
    } else if( keyboard.eventMatches(event,"w") ) { 
      controller.up = true; 
    } else if( keyboard.eventMatches(event,"a") ) { 
      controller.left = true; 
    } else if( keyboard.eventMatches(event,"d") ) { 
      controller.right = true;
    }
    setDirection( controller );
}

function onKeyUp( event ) {

    if ( keyboard.eventMatches(event,"s") ) {
      controller.down = false; 
    } else if ( keyboard.eventMatches(event,"w") ) { 
      controller.up = false; 
    } else if ( keyboard.eventMatches(event,"a") ) { 
      controller.left = false; 
    } else if ( keyboard.eventMatches(event,"d") ) { 
      controller.right = false; 
    } 
    setDirection( controller );
}

keyboard.domElement.addEventListener('keydown', onKeyDown );
keyboard.domElement.addEventListener('keyup', onKeyUp );


// FPS
var fps = { 
  startTime : 0,  
  frameNumber : 0,  
  getFPS : function() {
    this.frameNumber++;
    var d = new Date().getTime(),
    currentTime = ( d - this.startTime ) / 1000,
    result = Math.floor( ( this.frameNumber / currentTime ) );
    if( currentTime > 1 ) {
      this.startTime = new Date().getTime();
      this.frameNumber = 0;
    }
    return result;
  }
};
var f = document.getElementById( 'framerate' );


// COUNTDOWN TIMER
var startTimer = function ( duration, display ) {
  var timer = duration / 1000;
  var minutes;
  var seconds;
  inactiveTime = setInterval( function(){
    minutes = parseInt( timer / 60, 10 );
    seconds = parseInt( timer % 60, 10 );
    
    // adding leading zeroes
    minutes = ( minutes < 10 )? "0"+ minutes : minutes
    seconds = ( seconds < 10 )? "0"+ seconds : seconds;
    display.textContent =  minutes + ":" + seconds;
    timer = timer - 1;

    if( timer < 0 && !state ){
      clearInterval( inactiveTime );
      update = false;
      document.getElementById( 'count' ).innerHTML = "TIMES UP! :(";
      document.getElementById( 'count' ).style.display = 'block';
      document.getElementById( 'menu' ).style.display = 'block';
    }
  }, 1000);
};
var clock = document.getElementById( 'timer' );


// DISPLAY MAP
// var maps = document.getElementById( 'map' );

var update = false;

var id;
var render = function() {
  if ( update ) {
    f.innerHTML = fps.getFPS() + " FPS";

    spinOnAxis();
    moveEnemies();
    collision();
    
    // move the character
    if ( body.direction.x !== 0 || body.direction.z !== 0 ) {
      rotate();
      moveCharacter();
    }

    // set the camera to look at and follow the character
    camera.position.set( body.position.x, body.position.y + 8 , body.position.z - 32  );
    camera.lookAt( body.position );
  }

  if ( state ) {
    clearInterval( inactiveTime );
    if ( body.position.y > 50 ){
      document.getElementById( 'count' ).innerHTML = "YOU DID IT! :)";
      document.getElementById( 'count' ).style.display = 'block';
      document.getElementById( 'panel' ).style.display = 'none';
      document.getElementById( 'menu' ).style.display = 'block';
    } else {
      body.position.y += 0.08;
      body.rotateOnAxis( axis, 0.01 );
      camera.lookAt( body.position );   
    }
  }

  id = requestAnimationFrame(render);
  renderer.render(scene, camera);
}

var newGame = true;
function start ( target ) {
  var count = 3;

  document.getElementById( 'intro' ).style.display = 'none';
  
  var interval = setInterval( function() {
    count--;

    if (count < 0) {
      document.getElementById( 'count' ).style.display = 'none';
      document.getElementById( 'intro' ).style.display = 'none';
      document.getElementById( 'panel' ).style.display = 'block';
      
      update = true;
      
      if ( newGame ) {
        startTimer( 90*1000, clock ); // start timer
        newGame = false;
      }
      
      render();
      
      clearInterval( interval );
    } else {
      document.getElementById( 'count' ).style.display = 'block';
      var text;
      switch (count) {
        case 2: text = "READY"; break;
        case 1: text = "SET"; break;
        case 0: text = "GO"; break;
      }
      document.getElementById( 'count' ).innerHTML = text;
    }
  }, 1000 );
}


function init () {
  update = false;
  document.getElementById( 'count' ).style.display = 'none';
  document.getElementById( 'menu' ).style.display = 'none';
  window.location.reload( true ); 
  document.getElementById( 'intro' ).style.display = 'block';
}

render();
